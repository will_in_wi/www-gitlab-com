---
layout: job_family_page
title: VP of Recruiting
---

## VP of Recruiting

The VP of Recruiting is responsible for creating and overseeing the overall strategy of the Recruiting team. The VP of Recruiting reports to the Chief People Officer and works closely with the organizational leaders in shaping GitLab's recruiting strategies to support hiring targets, innovation and rapid growth. The VP of Recruiting uses their deep experience in Recruiting areas of organizational strategy, workforce planning, candidate experiences, and compliance with hiring policies and procedures.

## Responsibilities

* Partners with the E-Group to determine strategic landscape for our geographical dispersion
* Partners with the E-Group to align on overall plan for talent acquisition to ensure we maintain the high caliber of team members
* Partner with Finance and Department Leaders to deliver data-driven workforce planning processes to ensure the right people are in the right roles with a clear view of talent supply and demand
* Champion the diversity efforts within GitLab to drive a diverse talent pool for our open roles
* Provide leadership to the Global Recruiting team by creating a culture of accountability with a focus on delivering measurable results
* Continuous partnership with the CPO on the structure and capabilities required of the Recruiting team
* Drive an engaging team culture based on GItLab's core values
* Consistently evaluate and evolve team structure to support growing business needs
* Responsible for accurate and transparent hiring plans that are publicly accessible in real time

## Requirements
* Proven experience in high growth environment
* Successfully driven globally distributed hiring
* Excellent people management skills, enabling the Recruiting team to reach their full potential
* Led a high performing Recruiting team
* Strong attention to detail and ability to work well with changing information
* Comfortable using including a working knowledge of GitLab
* Succinct communication skills with the ability to collaborate with cross functional team members
* A team player who acts with a sense of urgency and adapts to a fast-paced and ever-changing environment
* Resourceful and takes initiative to seek internal and external resources when problem solving
* Have implemented recruiting tools while delivering internal OKRs
* 10+ years recruiting experience with a minimum of 2 years managing a recruiting team.
* Experience hiring Global Talent in areas like EMEA and APAC
* Ability to use GitLab

## Performance Indicators

* [Active Users with a Recruiting or Hiring Manager LinkedIn Seat](/handbook/hiring/metrics/#active-users-with-a-recruiting-or-hiring-manager-linkedin-seat--x)
* [Average Location Factor](/handbook/people-group/people-operations-metrics/#average-location-factor)
* [Candidate ISAT](/handbook/hiring/metrics/#interviewee-satisfaction-isat)
* [Candidates Sourced vs. Candidates Hired](/handbook/hiring/metrics/#candidates-sourced-vs-candidates-hired)
* Cost Per Hire
* [Hires vs. Plan](/handbook/hiring/metrics/#hires-vs-plan)
* [Offer Acceptance Rate](/handbook/hiring/metrics/#offer-acceptance-rate)
* [Pay Equality](/company/culture/inclusion/#performance-indicators)
* [Recruiting or Hiring Manager LinkedIn Seat](/handbook/hiring/metrics/#recruiting-or-hiring-manager-linkedin-seat--x)
* [Sourced vs Applied Candidates](/handbook/hiring/metrics/#sourced-vs-applied-candidates)
* [Time to Offer Accept](/handbook/hiring/metrics/#time-to-offer-accept-days)
* [Women in Company](/company/culture/inclusion/#performance-indicators)
* [Women in Leadership](/company/culture/inclusion/#performance-indicators)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/company/team/).

   * Qualified candidates will be invited to schedule a 30 minute screening call with a GitLab Recruiter
   * Then, candidates will be invited to schedule two separate 50 minute interviews; a Recruiting Manager, and the Director, Global People Operations
   * Next, candidates will be invited to schedule 4 separate 50 minute interviews; VP of Field Operations, VP of Engineering, Chief Legal Officer, and the Chief Financial Officer
   * Finally, candidates will be invited to a 50 minute interview with the CEO

As always, the interviews and screening call will be conducted via a [video call](https://about.gitlab.com/handbook/communication/#video-calls). See more details about our interview process [here](https://about.gitlab.com/handbook/hiring/interviewing/).
