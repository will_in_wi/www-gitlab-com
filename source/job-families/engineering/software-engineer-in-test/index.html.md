---
layout: job_family_page
title: "Software Engineer in Test"
---

GitLab is looking for a motivated and experienced engineer to help grow our test automation efforts across the entire GitLab ecosystem.
This is a key position with a new and growing team, so your efforts will have a noticeable impact to both the company and product.
In addition to the requirements below, successful candidates will demonstrate a passion for high quality software,
strong engineering principles, and methodical problem solving skills.

## Responsibilities

- Expand our existing test automation framework and test coverage.
- Develop new tests and tools for our GitLab.com frontend, backend APIs and services, and low-level systems like geo replication,
CI/CD, and load balancing.
- Have working knowledge of GitLab provisioning and setup tools like Terraform and Ansible.
- Setup and maintain new GitLab test environments.
- Participate in test pipeline triage rotation and ensure that pipeline failures are triaged in a timely manner.
- Have working knowledge of the entire GitLab application stack including tests in all levels. (Unit, Integration and End-to-end)
- Groom and prune GitLab tests in all levels to ensure optimal coverage and effective deduplication.
- Work with the product team and other development teams to understand how new features should be tested,
and then engage them in contributing automated tests.
- Work with engineers to define and implement mechanisms to inject testing earlier into the software development process.
- Identify and drive adoption of best practices in code health, testing, testability, and maintainability.
You should know about clean code and the test pyramid, and champion these concepts.
- Analyze complex software systems and collaborate with others to improve the overall design, testability and quality.
- Strive for the fastest feedback possible. Test parallelization should be a top priority.
You see distributed systems as a core challenge of good test automation infrastructure.
- Configure automated tests to execute reliably and efficiently in CI/CD environments.
- Track and communicate test results in a timely, effective, and automated manner.


## Requirements

- Strong experience developing in Ruby
- Strong experience using Git
- Experience using test automation tools like Capybara, Watir, Selenium
- Relevant work experience in software development and/or test automation
- Experience working with Docker containers
- Experience with AWS or Kubernetes
- Experience with Continuous Integration systems (e.g., Jenkins, Travis, GitLab)
- Ability to use GitLab


## Junior Software Engineer in Test

1 or more years of software engineering experience in Test Automation, Test Tooling and Infrastructure, or Development Deployment Operations.

* **Responsibilities**
  *  Implement basic test automation and continuous integration given designs and help from other team members.
  *  Maintain existing test automation framework.
  *  Maintain our CI system.
* **Planning & Organization**
  * Organize and complete structured assignments.
* **Independence & Initiative**
  * Work under general direction on problems of limited scope; use discretion to complete tasks.

## Intermediate Software Engineer in Test

3 or more years of software engineering experience in Test Automation, Test Tooling and Infrastructure, or Development Deployment Operations.

* **Responsibilities** (extends the responsibilities of the previous level)
  * Implement test automation for new features with little guidance.
  * Occasionally lead test automation efforts on new features.
  * Participate in test plan discussion.
  * Participate in architectural discussions.
  * Participate in design reviews with product management and engineering teams.
  * Maintain test infrastructure stability in production and non-production environments.
* **Planning & Organization**
  * Establish deadlines and approach for completing some assignments; demonstrates project management skills as part of larger team.
* **Independence & Initiative**
  * Work on problems of moderate scope; exercises judgment and independently identifies next steps.

## Senior Software Engineer in Test

7 or more years of software engineering experience in Test Automation, Test Tooling and Infrastructure, or Development Deployment Operations.


* **Responsibilities** (extends the responsibilities of the previous level)
  * Lead test automation implementation and provide guidance on testing approach for new feature development.
  * Create test plans for new features and steer the team to ensure test coverage based on the test plan.
  * Identify test gaps and prioritize adding coverage based on areas of risk.
  * Provide input into testing the security and scalability of the product.
  * Lead development of new tooling and infrastructure.
  * Implement new automation framework features with little guidance.
  * Recommend new test automation tools and processes that will improve our quality and velocity.
  * Take ownership of test failures and ensure that our CI system is reliable.
  * Mentor other engineers.
  * Occasionally contribute to the company blog.
* **Planning & Organization**
  * Independently and regularly manage project schedules ensuring objectives are aligned with team/department goals.
* **Independence & Initiative**
  * Work on problems of diverse scope requiring independent evaluation of identifiable factors; recommend new approaches to resolve problems.

## Staff Software Engineer in Test

10 or more years of software engineering experience in Test Automation, Test Tooling and Infrastructure, or Development Deployment Operations.

* **Responsibilities** (extends the responsibilities of the previous level)
  * Lead test automation infrastructure implementation across multiple product areas.
  * Regularly lead discussions on architectural improvements of test tooling and infrastructure.
  * Regularly consulted on testing approach for new feature development.
  * Analyze engineering metrics and make suggestions to improve engineering processes and velocity.
  * Participate in customer calls and take part in Engineering outreach.
  * Regularly review new content from the department added to the company blog.
* **Planning & Organization**
  * Define and independently manages multiple projects within the department.
* **Independence & Initiative**
  * Work on complex department issues that have an impact on the company's bottom-line. Able to create new methods for obtaining results.

## Performance Indicators

Software Engineers in Test have the following job-family performance indicators.

* [Average CE/EE master end-to-end test suite execution duration per month](/handbook/engineering/quality/performance-indicators/#average-ce-ee-master-end-to-end-test-suite-execution-duration-per-month)
* [Ratio of quarantine vs total end-to-end tests in master per month](/handbook/engineering/quality/performance-indicators/#ratio-of-quarantine-vs-total-end-to-end-tests-in-master-per-month)
* [Successful vs failed CE/EE master pipelines per month](/handbook/engineering/quality/performance-indicators/#successful-vs-failed-ce-ee-master-pipelines-per-month)
* [New issue first triage SLO](/handbook/engineering/quality/performance-indicators/#new-issue-first-triage-slo)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below.
Please keep in mind that candidates can be declined from the position at any stage of the process.
As a result an interview can be canceled at any time even if the interviews are very close (e.g. a few hours apart).

To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Candidates will be invited to complete a technical assessment from our Recruiting team.
- Qualified candidates will be invited to schedule a 30 minute screening call with our Recruiting team.
- Next, candidates will then be invited to schedule a 1 hour technical interview with a Senior Software Engineer in Test.
- Next, candidates will then be invited to schedule a 1 hour peer interview with two Engineers in the Quality department.
- Next, candidates will be invited to schedule a 45 minute first interview with the Hiring Manager.
- Finally, candidates will be invited to schedule a 45 minute interview with the Head of Quality.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).
