export const formatAmount = function (amount, currencyCode) {
  const formattedAmount = amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');

  if (currencyCode) {
    return formattedAmount + ' ' + currencyCode;
  }

  return '$' + formattedAmount;
};
