---
layout: handbook-page-toc
title: "Frontend Group"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Teams

* [Configure](/handbook/engineering/frontend/configure/)
* [Create](/handbook/engineering/frontend/create/)
* [Manage](/handbook/engineering/frontend/manage/)
* [Monitor](/handbook/engineering/frontend/monitor/)
* [Plan](/handbook/engineering/development/dev/fe-plan/)
* [Secure](/handbook/engineering/frontend/secure/)
* [Verify and Release](/handbook/engineering/frontend/verify-release/)

### Frontend domain experts

Do you have a question concerning the frontend for a product area? As a product manager do you want to run a new idea by a frontend engineer? Do you want to learn more about how the current implementation works? Do you have specific frontend technology questions or have some idea/inputs?

**Wait no more, here are our frontend domain experts who can help you out**
- CI/CD + Security product - [Filipa Lacerda](/company/team/#FilipaLacerda)
- Discussion + MR View - [Fatih Acet](/company/team/#fatihacet)
- Design Management - [Natalia Tepluhina](/company/team/#ntepluhina)
- Portfolio Management + Geo - [Kushal Pandya](/company/team/#Kushal_Pandya)
- Web IDE - [Denys Mishunov](/company/team/#mishunov), [Paul Slaughter](/company/team/#pslaughter)

Technical:
- Security - [Filipa Lacerda](/company/team/#FilipaLacerda)
- Testing - [Winnie Hellmann](/company/team/#winh)
- UI Components - [Clement Ho](/company/team/#ClemMakesApps)
- Webpack + Tooling - [Mike Greiling](/company/team/#mikegreiling)
- NPM Dependency Janitor - [Lukas Eipert](/company/team/#leipert)

**They are responsible for**
- Person to contact about a specific topic
- Analyzing and Estimation of Deliverables
- Active Deliverable Creation (especially technical topics)
- Analysis of topics in that area (by example Research Linters for Security Checks)
- Active Bug tracking of bugs in that area

### Frontend group calls

The frontend group has scheduled weekly calls every Tuesday, before the company call. During this call, team members are encouraged to share
information that may be relevant to share with other members synchronously (Eg. new documentation change, new breaking changes added to `master`).

To help identify possible conflict areas in a release we encourage members to post a link to a comment in an issue that you’re working on that crosses the borders of the domain of your stage group that you feel has the potential of colliding with the work of others.

#### Frontend themed call

We started in January 2018, with adding a monthly theme to the first call of each month. The purpose of these themes is to add some
fun and quirkiness to our team calls (so that we can learn more about each other) but it shouldn't distract or derail into too much off-topic conversation.

**How does it work**
- Winner of the previous theme will determine the theme of the next month
- Theme will be announced a few weeks before the next month
- Everyone in the call should write down who they think is the winner on a piece of paper and reveal it all at once (to prevent voting bias) during the voting

| Date | Theme | Winner |
|---|---|---|
| 2018-01-09 | Most interesting hat | Jose Ivan Vargas |
| 2018-02-13 | Most interesting shirt/t-shirt (No GitLab swag) | Winnie |
| 2018-03-06 | Most interesting sunglasses | André |
| 2018-04-03 | Most peculiar trip souvenir | Eric Eastwood |
| 2018-05-08 | Grow the best grass or plant (preferably grass) | Lukas |
| 2018-06-04 | Most interesting sculpture from one piece of paper (maximum size Letter/A4) | Sam |
| 2018-07-10 | Ornament/Trinket/Thing-that-you-have, that says the most about you | Sam |
| 2018-08-07 | Most creative Tanuki in any medium | Eric Eastwood |
| 2018-09-04 | Best animal sketch/drawing | Winnie's wife |
| 2018-10-02 | Favorite mythical creature of your home country | Tim / Lukas |
| 2018-11-06 | Convince us that your favorite dish (that you can prepare yourself) is the best! [Preferably you share the recipe](https://gitlab.com/leipert-projects/gitlab-recipes) | Mike |
| 2018-12-04 | Favorite multi-player game (board game, card game, video game, etc). Describe the rules, how many players, background, and why you like it | Winnie |
| 2019-01-08 | Favorite children's book | [Nathan](https://gitlab.com/nfriend) |
| 2019-02-05 | Best feature of your house/office/co-working space.  If possible, show it! <br /><br /> Some examples:<br /> - The ocean view from your living room window<br /> - Your room exclusively dedicated to Star Wars memorabilia<br /> - The secret passage you built behind a bookcase | [André](https://i.imgur.com/9PUJ4oL.jpg) |
| 2019-03-05 | Favourite music album of all time? | Fatih |
| 2019-04-02 | Best picture you took (Feel free to post-edit) | [André](https://imgur.com/a/9fpJvt4) |
| 2019-05-07 | _skipped due to GitLab Contribute_ | |
| 2019-06-04 | What's your favourite movie? | [Sam](https://i.imgur.com/OjYQglp.jpg) |
| 2019-07-02 | Show us your pets | Sarah |
| 2019-08-06 | Share your favorite piece of art: painting, photograph, sculpture, installation, etc. | [Himanshu](https://www.behance.net/gallery/60331507/The-Book-of-Dragons-Illustrations) |
| 2019-09-03 | What is your favourite "What is your favourite?" question? What would you ask? How would you answer it yourself? | Sarah |
| 2019-10-01 | What is your favorite thing to complain about? | Lukas |
| 2019-11-12 | What is the best dish you have ever eaten at a restaurant? | Dennis |
| 2019-12-10 | What is your favorite thing about your hometown? It can be a little-known secret, awesome restaurant, famous story | ? |


### Frontend calendar

The frontend Google Calendar was created as a means of unifying and including team members on frontend related discussions.
Frontend meetings should be listed on the calendar unless there is a good reason not to.
Frontend engineers have the calendar permissions to create events on this calendar while GitLab team-members have the permissions to add the frontend calendar to their calendar.

> Note: Due to a bug on Google Calendar, team members are unable to find the calendar by searching.
Team members are recommended to go to the [frontend calendar link](https://calendar.google.com/calendar/b/1?cid=Z2l0bGFiLmNvbV83dHQ0bDA1NWg1MGRndWNpZDBidGg1ZGZsOEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t)
on their browser to add this calendar to their calendar. This calendar is currently only available within GitLab and is not publicly accessible.

### RFCs for changes to GitLab Frontend
If you would like to propose a new, or a change to an existing, frontend pattern in our codebase, please create an issue in [https://gitlab.com/gitlab-org/frontend/rfcs](https://gitlab.com/gitlab-org/frontend/rfcs) so that we can discuss it.

### Videos

#### Frontend Skillsharing Initiative

A good way to learn from the collective wisdom of our Frontend team is by checking out the videos available on [the Skillsharing Initiative page](/handbook/engineering/frontend/skillsharing/).

#### Frontend Onboarding

Here are some helpful resources that might help your onboarding.

- [GitLab: Frontend architecture at GitLab](https://youtu.be/LnyeMjTauxQ)
- [GitLab: From Legacy to Vue.js](https://youtu.be/qJFrjiQ7nr0)
- [How We Do Vue At GitLab](https://youtu.be/EsUS4JX0BBw)

### Technical Interview Metrics

Learn more about how we measure our technical interview metrics in [the interview metrics page](/handbook/engineering/frontend/interview-metrics/)
