---
layout: handbook-page-toc
title: "Sales Development"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

As a Sales Development Representative (SDR) you will be dealing with the front end of the sales process. Your focus will be on generating opportunities that the Small Business (SMB) Advocates, Account Executives (AE) and Strategic Account Leader (SAL) accept, ultimately leading to closed won business. On this team we work hard, but have fun too (I know, it's a cliche ...but here it's true!). We will work hard to guarantee your success if you do the same.

This handbook lays out the knowledge you will need to have in order to be successful in your role here at GitLab. Know that if you have the right mindset and come to work hungry with a positive attitude, you will see growth personally and in your career. Let's make it happen!

### Slack Channels & Aliases   

*You must be logged into your GitLab slack account for the links below to be active*

**Main Channel** = [`#sdr_global`](https://gitlab.slack.com/messages/C2V1KLY0Z)      
**Enablement** = [`#sdr-enablement`](https://gitlab.slack.com/messages/CCV9KKAQY)       
**Conversations** = [`#sdr-conversations`](https://gitlab.slack.com/messages/CD6NDT44F)       

#### AMER
**All** = [`#sdr_amer`](https://gitlab.slack.com/messages/CM2GAVC78)      
**Commercial + LATAM** = [`#sdr_amer_commercial`](https://gitlab.slack.com/messages/CM276FD2B)        
**East** = [`#sdr_amer_east`](https://gitlab.slack.com/messages/CGTF184EB)      
**Central** = [`#sdr_amer_central`](https://gitlab.slack.com/messages/CLXHF3A04)      
**West** = [`#sdr_amer_west`](https://gitlab.slack.com/messages/CLU1A6BA5)      

#### EMEA
**All** = [`#sdr_emea`](https://gitlab.slack.com/messages/CCULKLB71)      
**Commercial** = [`#sdr_emea_commercial`](https://gitlab.slack.com/messages/CM0BYV7CM)      

#### APAC
**All** = [`#sdr_apac`](https://gitlab.slack.com/messages/CM0BPBEQM)



### Issue Boards & Team Labels 

SDRs use the [SDR Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/707128) to track relevant GitLab issues involving the SDR team. This is a global issue board and will capture all issues in any group/sub-group in the GitLab.com repo when any of the following *scoped* labels are used. 

- `SDR::Review` - issues concerning the SDR team
- `SDR::Discussion` - SDR related issues that are being discussed currently within the SDR org
- `SDR::To Do` - SDR org has made a decision to move forward with this project but haven’t started on it yet
- `SDR::Doing` - SDR org is actively working on this project
- `SDR::Priority` - projects that we would like brought into RevOps meeting for feedback/next steps from other teams
- `SDR::On Hold` - SDR project is put on hold after agreement from SDR leadership team 
- `SDR::Blocked` - SDR org is blocked from taking any further action by a team/person outside of SDR org

## GitLab Buyer
- [Video: Understand the GitLab Buyer: Personas and Pain Points](https://www.youtube.com/watch?v=-UITZi0mXeU&feature=youtu.be)
- [GitLab Roles and Personas](/handbook/marketing/product-marketing/roles-personas/)
- [Video: Chief Architect/Head of DevOps](https://www.youtube.com/watch?v=qyELotxsQzY)
- [Video: Head of IT](https://www.youtube.com/watch?v=LUh5eevH3F4)
- [Video: DevOps Director/Lead](https://www.youtube.com/watch?v=5_D4brnjwTg)

## GitLab Industry
- [Video: Understand the industry in which GitLab competes](https://www.youtube.com/watch?v=qQ0CL3J08lI)
- [DevOps Lifecycle](/stages-devops-lifecycle/)
- [Glossary of Terms](https://docs.gitlab.com/ee/university/glossary/)
- [GitLab Market and Ecosystem](https://www.youtube.com/watch?v=sXlhgPK1NTY&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=6)

### DevOps
- [Video: What is DevOps?](https://www.youtube.com/watch?v=_I94-tJlovg)
- [Video: Why Auto DevOps Really Matters](https://www.youtube.com/watch?v=56chpCftwJQ&feature=youtu.be)
- [The State of DevOps 2017](https://www.bmc.com/content/dam/bmc/migration/pdf/Interop+ITX+State+of+DevOps_Final2-002-V1.pdf)
- [Video: Keynotes - DevOps Enterprise Summit 2017](https://www.youtube.com/playlist?list=PLvk9Yh_MWYuyq_rn0H-AP3ORoQ1QM0s1L)

### Competitive Landscape
- [Video: GitLab Competitive Landscape](https://www.youtube.com/watch?v=GDqGO5cv1Mk)
- [GitLab Compared](/devops-tools/)

## GitLab Solution
- [Video: Understand The GitLab Value Proposition](https://www.youtube.com/watch?v=6Dsdd1LSf40)
- [Video: Where GitLab Fits in Within Current Tool Stack](https://www.youtube.com/watch?v=YHznYB275Mg&feature=youtu.be)
- [GitLab Positioning](/handbook/positioning-faq/)
- [Customer Use-Cases](/handbook/use-cases/)
- [Customer Stories](/blog/categories/customer-stories/)
- [GitLab Sales Demo](/handbook/marketing/product-marketing/demo/)
- [Video: Converting Free to Paid](https://www.youtube.com/watch?v=56eRRl6zFuA&feature=youtu.be)
- [Video: GitLab Sales Training - Security Products](https://www.youtube.com/watch?v=O1XFOlnywDI&feature=youtu.be)

## GitLab Messaging
Understand how to connect the GitLab buyer pain points to your calls, email messaging & social strategies

- [GitLab Elevator Pitch](/handbook/marketing/product-marketing/messaging/#gitlab-value-proposition)
- [Video: Successful Messaging](https://www.youtube.com/watch?v=8LDlcvOgn9w)
- [Questions to Ask Yourself Before Sending an Email](/handbook/sales-training/#6-questions-sales-reps-should-but-dont-ask-themselves-before-sending-an-email-)
- [How to Write the Perfect Sales Email](https://www.saleshacker.com/how-to-write-the-perfect-sales-email/)
- [Voicemail Tips](https://blog.hubspot.com/sales/sales-voicemail-tips-that-guarantee-callbacks)
- [3 No Rule](https://docs.google.com/document/d/1aXeFKzwu5eGgGI6x_JrljIDL_ssTZrA7M7o0sAFNXVw/edit)

## How & When to Create an Opportunity

Process to follow how & when to create an opporunity is found in the [Business OPS](/handbook/business-ops/resources/#how-to-create-an-opportunity) section of the handbook. 


## Training and Resources

You play a crucial role that helps bridge the gap between sales and marketing. As you gain knowledge, you will be able to aid our future customers ship better software, faster. There are numerous resources at your fingertips that we have created to help you in this process. Should you require any further training throughout your time with Gitlab, this can always be arranged to plug any gaps in your knowledge around the product or SDR processes.

### Onboarding
You  will be assigned a GitLab Onboarding Issue. If you complete the SDR onboarding program within the defined time, you will receive the Super Leaf Coin. The SDR onboarding program provides you with the essential tools and and information in order to be successful as a SDR. The onboarding contains a prescriptive process and set of tasks for each day of your first 30 days of employment.

**Onboarding Assessments**

TBD

### Super Leaf Coin

When you've successfully passed your onboarding test you will receive this Super Leaf Challenge Coin because you now have eaten [The Super Leaf](https://www.mariowiki.com/Super_Leaf).

![Super Leaf Challenge Coin](/images/handbook/marketing/super-leaf-challenge-coin.png)

More info about why this skill is so helpful to the tanuki is explained in the first item on [this page](https://en.wikipedia.org/wiki/Japanese_raccoon_dog#In_popular_culture).

### Process Resources

- [Business Operations Handbook](/handbook/business-ops/)
- [Business Operations **Resources**](/handbook/business-ops/resources/)
- [Marketing Handbook](/handbook/marketing/)
- [Sales Handbook](/handbook/sales/)
- [Customer Success Handbook](/handbook/customer-success/)
- [Resellers Handbook](/handbook/resellers/)
- [Support Handbook](/handbook/support/)

### Product Resources

- [GitLab Resources](/resources/)
- [GitLab Blog](/blog/)
- [GLU GitLab University](https://docs.gitlab.com/ee/university/)
- [GitLab Primer](/company/)
- [Sales Qualification Questions](/handbook/sales-qualification-questions/)
- [FAQ from Prospects](/handbook/sales-faq-from-prospects/)
- [Platzi GitLab Workshop](https://courses.platzi.com/classes/git-gitlab/)
- [GitLab Documentation](http://docs.gitlab.com/ee/)
- [Version.gitlab](https://version.gitlab.com/users/sign_in)
- [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
- [Additional Training Resources](https://drive.google.com/drive/folders/0B1W9hTDXggO1NGJwMS12R09lakE)

### Tools

- [Tech Stack](/handbook/business-ops/tech-stack-applications/#tech-stack-applications)
- [Tools and Tips](/handbook/tools-and-tips/)
- [Getting Started with Slack](https://get.slack.help/hc/en-us/articles/218080037-Getting-started-for-new-members)
- [Outreach University](https://university.outreach.io/)
- How to Use Outreach - updated process coming soon
- [Outreach Killer Content & Using Sequences](https://university.outreach.io/live-training/80859)
- [Outreach Executing Tasks with Precision](https://university.outreach.io/live-training/80896)
- [Video: Outreach Advanced Training](https://drive.google.com/open?id=0BxIvWVJUiX7AZGNWdEZ5MHpCOHc)
- [Drift Agent Training Guide](https://help.drift.com/getting-started/drift-agent-training-guide)
- [Salesforce Trailhead Training](https://www.salesforce.com/services/training/overview/)
- [Video: LinkedIn Sales Navigator Training](https://www.youtube.com/watch?v=jxPw_pXqd2s&feature=youtu.be)
- [Drift Agent Training Guide](https://help.drift.com/getting-started/drift-agent-training-guide)
- [Drift Starter Guide](https://help.drift.com/getting-started/228660-the-ultimate-drift-starter-guide-for-sales-teams)

### Personal Development
- [Video: Time Management for Sales Development Success](https://www.youtube.com/watch?v=bA_xdjwAfZM&feature=youtu.be)
- [Video: How to Boost Your Business Acumen](https://www.youtube.com/watch?v=-h6XciVfraI)
- [Video: Business Acumen for Sales Professionals](https://www.youtube.com/watch?v=XRt9RIvxUTA)
- [Fanatical Prospecting](https://www.amazon.com/Fanatical-Prospecting-Jeb-Blount/dp/8126560053/ref=tmm_pap_swatch_0?_encoding=UTF8&qid=&sr=)
- [Predictable Revenue](https://www.amazon.com/Predictable-Revenue-Business-Practices-Salesforce-com/dp/0984380213)
- [Predictable Prospecting: How to Radically Increase for B2B Sales Pipeline](https://www.amazon.com/Predictable-Prospecting-Radically-Increase-Pipeline/dp/1259835642/ref=sr_1_3?ie=UTF8&qid=1516560864&sr=8-3&keywords=prospecting)
- [The Challenger Sale](https://www.amazon.com/Challenger-Sale-Control-Customer-Conversation/dp/1591844355/ref=sr_1_1?ie=UTF8&qid=1516740978&sr=8-1&keywords=the+challenger+sale)
- [The Challenger Customer](https://www.amazon.com/Challenger-Customer-Selling-Influencer-Multiply/dp/1591848156/ref=sr_1_3?ie=UTF8&qid=1516740978&sr=8-3&keywords=the+challenger+sale)
- [Smart Calling: Eliminate the Fear, Failure, and Rejection from Cold Calling](https://www.amazon.com/Smart-Calling-Eliminate-Failure-Rejection-ebook/dp/B00C2BR56W)
- [Sales EQ: How Ultra High Performers Leverage Sales-Specific Emotional Intelligence to Close the Complex Deal](https://www.amazon.com/Sales-Performers-Sales-Specific-Emotional-Intelligence/dp/B073HH4WPR/ref=sr_1_36?ie=UTF8&qid=1516560951&sr=8-36&keywords=prospecting)
- [High Profit Prospecting: Powerful Strategies to Find the Best Leads and Drive Breakthrough Sales Results](https://www.amazon.com/High-Profit-Prospecting-Powerful-Strategies-Breakthrough/dp/0814437761/ref=sr_1_4?ie=UTF8&qid=1516560899&sr=8-4&keywords=prospecting)
- [New Sales. Simplified.: The Essential Handbook for Prospecting and New Business Development](https://www.amazon.com/New-Sales-Simplified-Prospecting-Development-ebook/dp/B0094J7S9Y/ref=sr_1_2?ie=UTF8&qid=1516560864&sr=8-2&keywords=prospecting)
- [Hacker News](https://news.ycombinator.com/)
- [Martin Fowler](https://martinfowler.com/)
- [The New Stack](https://thenewstack.io/)
- [Top 13 DevOps Blogs You Should Be Reading](https://stackify.com/top-devops-blogs/)

## SDR Manager

### Weekly 1:1
- Current progress of onboarding program, if applicable
- Mental check-in (winning & success)
- Dashboard update & metrics overview
- Current prioritization & goals
- Coaching - email strategy, tools, campaigns, cadence, best practices (review recordings, if applicable)
- Review goals at the account level and personal level
- Follow up on action items from past meetings
- Upcoming events/campaigns that can be leveraged
- Personal goals & commitments
- Strategy for next week
- Recap and plan for next 1:1


## Asking questions

Don't hesitate to ping one of your colleagues with a question, or someone on the team who specializes in what you're searching for. Everyone is happy to help, and it's better to get your work done faster with your team, than being held up at a road block.
- [DevOps Stages on the Product stages, groups, and categories page](/handbook/product/categories/#devops-stages)

## Referring People to the Sales and Business Development Teams

Know someone who works at a company with at least 750 technical employees? Please introduce them to a Sales Development Representative Manager. When an SDR receives an introduction like this, they will respectfully ask the person that was introduced to them if they would be willing to point them to any people who are responsible for evaluating or selecting SCM, CI/CD, application monitoring, and/or software project management solutions. This approach helps ensure we're talking to the right people, and helps reduce the time it takes to identify the people within a company who would be best suited to hear about how GitLab can help their organization ship better software, faster.

You can use the following text to make an introduction over email, (please copy the SDR Manager on the email):

```
Hi {contact.first.name},

My colleague {sdr.mgr.first.name} has been trying to find people responsible for evaluating and selecting DevOps tools at {contact.organization}. Could you do me a favor and point them in the right direction? 

They would be asking to schedule a quick call to discuss GitLab; the first single application for software development, security, and operations that enables Concurrent DevOps, making the software lifecycle 200% faster and radically improving the speed of business.

Thank you, I appreciate your help.

Best,

{first.name}
```

## How to SDR, Commercial

{:toc}

[Video: Welcome to the Sales Development Commercial Team](https://www.youtube.com/watch?v=XuTuYsD24ck&feature=youtu.be)

## SDR, Commercial Standards
- Meet monthly quota of Sales Accepted Opportunities (SAOs)
- Strategize to develop the proper qualifying questions for all types of prospective customers.
- Be able to identify where a prospective customer is in their buying cycle and take appropriate action to help them along their journey towards becoming a customer.
- Have a sense of urgency - faster response time directly influences conversion rates.
- Generate Sales Accepted Opportunities (SAOs)
- Work lead records within Salesforce by leveraging sequences in Outreach
- Maintain sense of ownership over data integrity & consistent activity logging
     - Includes adding complete **ACCOUNT** address details when an opportunity is created
     - Leveraging the data sources in order of enrichment priority (Datafox & DiscoverOrg)
- Do not touch any **LEAD** that has activity on it within the last 60 days by a different SDR. 
     - Conversely, if a LEAD you've been engaged with before, comes back in but now falls to a new SDR based on `Territory` changes, it needs to be transferred based on **new assignment rules** unless you have *ACTIVELY* engaged within the last 60 days.

### Flow
![SDR, Commercial Process Flow](/images/handbook/marketing/bdrFlow.png){: .shadow}

In this process you will be setting meetings with the prospect. Please make sure that you include a meeting agenda in those invitations. You can use the following text:

For invites sent to qualify a lead:

```
“Thanks for putting a brief call on the calendar to discuss your team's core focus/goals.”

Proposed agenda:

- Mutual Introductions

- Discuss your team’s workflow, needs, use cases

- Determine next steps (ex. AE led demo, technical resource support)
```

For Handoff to an Account Executive:

SDR should begin agenda by recapping what is known from previous emails/meetings, including whether they are currently using our platform in any capacity

```

Proposed agenda:

- Mutual introductions

- Discuss current tool stack and GitLab usage/relationship

- Understand team’s ideal workflow and determine where GitLab fits in

- Overview of GitLab application

- Determine next steps
```

It is imperative that you have an understanding of the following concepts to work effectively. Please be sure to review the following resources:

[Glossary](/handbook/business-ops/resources/#glossary)

[Customer Lifecycle](/handbook/business-ops/resources/#customer-lifecycle/)

[Lead Management](/handbook/business-ops/resources/#lead-management)

[Video: Lead/Contact Management - Disposition Status](https://www.youtube.com/watch?v=lLHax2VPqdE&feature=youtu.be)

[Rules of Engagement](/handbook/business-ops/resources/#rules-of-engagement)


## SDR Inbound Priority Process

### SDR Priority Process SFDC Work Views
#### P1 - MQLs-Request Contact
This will be your **top priority** view. Records with `Status` = `MQL` or `Inquiry` that have requested contact either via our web form or from an event. Review and determine what outreach/status should be. `Status` and/or `Initial Source` is not restricting factor when a contact request is received. **All** must be `Accepted`, follow up completed via the **SDR Request Contact - High Touch** sequence. Record then should be updated with appropriate `Status` disposition based on outcome. Expected follow up is to happen within 1 business day.


#### P2 - MQLs excl SaaS trials   
Records with `Status` = `MQL` AND `MQL Date` = **LAST 60 DAYS**. This could include leads that MQL'd due to a demo, downloaded resource or came by our booth at an event but this view will not include SaaS trials. Sort by `Last Interesting Moment Date` so they appear reverse chronological order (i.e. most recent at top). Sync records into Outreach enmasse, when possible, and insert into correct sequence located in the **SDR Inbound 2019** collection.  When you bulk import to Outreach, you will likely get a failure notice on some of the records which is an indicator of duplicates. Please follow the dupe and merge process before trying to import again. Trial leads need to be sequenced within 1-2 business as the sequence is coordinate with marketing's outreach.
*If you sort by the last column, you can see if any of these leads has both an Self-hosted and SaaS trials      


#### P3 - Qualifying 
Records with `Status` = `Qualifying` which means at some point, you may have been engaged with them. Decide on next steps and use the "qualification notes" field to enter in the next step date and action needed. This will help you to prioritize from the lead view itself.

#### P4 - MQL-Trial - SaaS
Records with `Status` = `MQL` AND `Trial - SaaS` = `True` w/in the last 60 days. Sort records by `Evaluators` and place in high or low trial sequence. You will find the related sequences in the **SDR Inbound 2019** collection. Trial leads need to be sequenced within 1-2 business as the sequence is coordinate with marketing's outreach.

#### P5 - Accepted, showing action
Records with `Last Intersting Moment Date` OR `Last Activity` in the **past 60 days**.   
Review and disposition the `Status` correctly:  
- If you are not still actively working the record, **update the `Status` = `Nurture`** but before doing so, change the `nurture reason` or the lead status will not be able to be saved.
- If records is unqualified, **update `Status` = `Unqualified`**  but before doing so, change the `unqualified reason` or the status will not be able to be saved.

You should sort by "Outreach stage" as an indicator of whether leads should be moved to 'Unqualified', 'Nurture', or 'Qualifying'. You can also sort by `Trial` columns to see if presently in or ever taken a trial/type. For example, if `Outreach stage` is `Unresponsive` and trial has ended, it likely should be moved to `Nurture`, `No Response`. 

You can leverage visible fields like `Title`, `No of Employees` and/or `Last Interesting Moment`/`Path Factory Engagement Score` to determine if record should remain in `Accepted` status for further outreach.

#### P6 - Accepted, no action  
Records *without* a `Last Interesting Moment` and/or `Last Activity` within the **last 60 days**. 

Review and decide if you need to take an action or if the lead needs it's status updated.  
The purpose of this view is to identify any bottlenecks, example: if behind on follow-up or if you forgot to disposition out of `accepted` status. It's good practice to check and clear this view once a week.

#### P7 - Inquiry  
Records with `Status` = `Inquiry` sort by `Last Interesting Moment Date` (LIM). This is a "fishing pond like" view.   
These records **have not yet** accumulated enough points (i.e. engaged with high value content) or completed a 'hand-raising' activity to be qualified as an `MQL`. 
Review the LIM description (if available) and `Company` to determine if worth bumping into an active outbound motion. There are related sequences to use in **SDR Inbound 2019** collection.   
Do not work to clear this view - this view will only be worked when P1 - P5 are completed.   

#### P8 - Stale MQL/Nurture w/new LIM
Records with `Status` = `MQL` OR `Nurture` AND `MQL Date` older than 30 days BUT `Last Interesting Moment Date` within 30 days or less.   
Do not work to clear this view - be aware of this view as records may appear in this view due to re-engagement campaigns. If there are leads in this view with a MQL status, you do need to decide on what action to take per our normal 1-2 business day SLA. 

*Note: If you see a lead in any of your views with a `Path Factory Engagement Score` of 5 or greater, we would like you to move to `accepted` and begin outreach unless you determine the lead to be `unqualified`. We will be testing whether this is the correct score to use and gathering feedback from the SDR team. You can find an appropriate sequence to use in the **SDR Inbound 2019** collection.

### SDR Record Handling Priority Process
Records - LEADS & CONTACTS - are generated by the Marketing team for SDR team who are responsible for qualifying the records **before** handoff to the Sales team. 


#### Qualification

Qualification is performed by the SDRs. **The best interest of the customer/prospect end user is to be paramount at all times**


#### Status Process
We must have an accurate reflection of the [record status](/handbook/business-ops/resources/#lead--contact-statuses)(lead & contact) based on what the person engaged with, what is the status of followup and where they are in the lifecycle. 

The SDR is **100% responsible** for ensuring this status is accurate.    


## SDR Lead Research
Researching leads is an important first step. Not only can you leverage the information to build a rapport, but it will ensure data integrity.

On leads that have a company email domain, you are responsible for updating the following fields to ensure accuaracy:
- First and Last name (be sure this is capalitalized)
- Title (Leverage LinkedIn button to quickly find) and populate LinkedIn URL 
- Number of Employees (Datafox is our #1 source of truth, DiscoverOrg is secondary)

- **If a lead is confirmed to be a part of an existing customer group, please be sure to convert to the affiliated account prior to reassigning.**   
- **If there are duplicate records in the system, it is our job to clean them up by merging LEAD records and converting them into existing CONTACTS, when applicable. When merging please retain the `Initial Source` that was created first as this is important to our attribution tracking.**
- **`Total Employee` count is one of fields on the LEAD that is used determine the `Sales Segmentation` for the LEAD. If the LEAD matches an ACCOUNT in Salesforce via LeanData the LEAD will adopt the Sales Segmentation of the ACCOUNT. If the LEAD does not match an ACCOUNT and the `Number of Employees` field on the LEAD is populated the Sales Segment of the LEAD will be determined by value in this field. If all other fields return an `Unknown` segment or are blank then the `Sales Segment` on the LEAD is determined by the value selected in the `Employee Bucket` field on the LEAD.**
   - This process runs concurrently to the way that [Sales Segments are determined on ACCOUNTS](/handbook/business-ops/resources/#segmentation). While this process aims to align the lead segment with the segment on the ACCOUNT discrepancies may occur. To avoid any issues please ensure that all LEADS have an ACCOUNT before converting them to CONTACTS. Once the ACCOUNT exists confirm the segment of the lead and who will be the owner of the soon to be created CONTACT based on the segment of the ACCOUNT. 

**Personalized message research help and example:**

Three things in three minutes about the Organization, Industry, and Prospect

**Salesforce**

**Under the ACCOUNT Record**
 * Is there an owned customer account?
 * Check for any alternative naming conventions
 * Are there any open opportunities?
 * Are there other leads from the organization that are being worked?
 * Who in the organization have we been in contact with?
 * Are there any duplicate leads or contacts?

**Under the LEAD Record**
 * Review Salesforce Activity History
 * Has anyone been in contact with this lead previously?
 * Review the widgets built into Salesforce for further information on lead's actions.
   * Lean Data
   *  Marketo Sales Insight
     * Interesting Moments
     * Web Activity
     * Emails (sent from Marketo)

**LinkedIn Sales Navigator**
* Confirm [Sales Segmentation](/handbook/business-ops/resources/#segmentation) by determining `Total Employee` count for buiness. **This is not an official data source, used for verification ONLY**
* Person Summary
* Does anything stand out that is relevant to their needs as an organization?
* Have they published any articles that would be worth referencing?
* What is the lead’s role and how does that affect your messaging?
   * [Buyer personas](#gitlab-buyer)
* Previous work
   * Any Gitlab Customers that you can reference?
* Are they connected to anyone at GitLab for a possible introduction?

**Company Website**
- Mission Statement
- Press Releases/Newsroom
- (ctrl F) search for keywords


### Working with 3rd Parties

**Gather billing and end user details from the reseller:**

- Billing company name/address:
- Billing company contact/email address:
- End user company name/address:
- End user contact/email address:
[Snippet in outreach](https://app1a.outreach.io/snippets/362)

**Create new LEAD record with end user details**

- Ensure that all notes are copied over to new LEAD.This is the LEAD that will be converted
- Determine who the account will go to based on the end user details.

**Converting LEAD**
- Opportunity should reflect there is a reseller involved. Opp naming convention: “End user account name via reseller account name-”. into an opportunity under this account owner.
- Determine who the opportunity will be routed to based on the end user details
- Opportunity should reflect there is a reseller involved. Opp naming convention: “End user account name via reseller account name-”. into an opportunity under this account owner.
- For the reseller LEAD, check to see if the reseller account is already in SFDC. If not, create one when converting LEAD into a CONTACT. Do not create a new opportunity.
- Assign record to the same account owner. Convert reseller into an contact on the reseller account.
- On the reseller contact, go to the activity. For each activity related to your opportunity,click edit, link it to the opp. Do this by choosing related opp and finding the new opportunity and hitting save.
- Once you have linked the activity to the opp, go to the new opportunity:
- Change the business type to new business and stage to pending acceptance.
- Under contacts, add the reseller contact, role as reseller, and primary contact.
- Under partners, add the reseller account as VAR/Reseller"


## SDR Prospecting
At GitLab, it is imperative that SDRs consistently follow up in a timely manner. Top performing SDR's will experiment with a mix of calls, personalized emails, and non-traditional touch points in their prospecting cadence.

### High vs Low Touch
SDRs should follow the [lead research process](/handbook/marketing/marketing-sales-development/sdr/#bdr-lead-research) to determine if the lead will be sequenced as high/low touch.

#### What distinguishes High from Low?

High Touch:

1.  Involves higher quality leads
1.  High Touch is for trial lead records where one of the following qualities is true: 
     * Persona title is given
     * Phone is given
     * In MM or Enterprise segment, unless relation to a midmarket/enterprise account is questionable
     * Has business email (if SB/Unknown segments, need to factor in 5+ users as a secondary qualifier)
     * Web form was completed with relevant question
1.  Involves personalization, more touches and multiple channels are used (calls,emails,LinkedIn)
1.  Offers a meeting as CTA early in the sequence whereas Low Touch will try to engage via email to allow SDR’s an opportunity to qualify before getting on a call that may not be a good use of their time. 

*SDR’s may weave in additional phone calls into their workflow


### Live Chat
We use a live chat tool - Drift - to facilitate two-way conversations with website visitors. As an SDR, you are expected to be active on Drift during your shift, with the exception of those times when you have to walk away or are taking a break. It is important to maintain a sense of urgency while watching the chat queue, sure to not miss any incoming inquiries.

See also: [Drift Starter Guide for Sales Teams](https://help.drift.com/getting-started/228660-the-ultimate-drift-starter-guide-for-sales-teams)

#### Drift Best Practices
You can see who else is online by viewing the [GitLab Team Profile](https://gitlabteam.drift.com/).

Notification settings:
- Go to `Browser Preferences` and select “Only conversations I’m in”.
- Desktop notifications will pop up every time you are added to the conversation.

When you are logged in and ready to chat, change your status to active by clicking “Turn off Away mode”

Pushing Conversation to Salesforce.com:
- Closing conversation when lead is qualified pushes conversation to Salesforce.com (either a new lead is created or conversation is attached to existing record).
- If you want to follow up with the site visitor, close the conversation, find record in SFDC and update necessary information.

Logging off:
- Go through all open conversations, mark the chats `Unqualified` if you are not following up with the site visitor, and close the conversation.
- “Set yourself to away” and close out window.

Conversation flow:
- Be helpful, but don’t allow yourself to become technical support. Refer to support as a paid feature, if necessary.
- Start conversations by obtaining name of the person and get an email if they haven’t supplied one. Determine company name as soon as possible.
- If the record is already assigned in Salesforce, alert the record owner and have them join the conversation, if possible.

Minor notes:
- If you leave the Drift page in your web browser, do not use the “back” button to return. The page will not be current. Follow a link or a bookmark. Be sure to hit the refresh button if you choose to use the “back” button.
- Drift conversations can be shared with others by copying the URL of the selected chat.


### Email
Tips
- Studies show that Executives read emails in the morning
- Expect to be forwarded to the right person (Always ask)
- Keep emails 90 words or less
- Break into readable stanzas
- Always be specific and tailored
- Make your emails viewable from a mobile device

1. Subject line
1. Preview pain
1. Opening stanza
    - Make it about them, not about you
1. Benefit and value proposition
1. Call to action (CTA)
    - Offer available times for a meeting/call
    - Create urgency

### Working with Trials
During a trial, a prospect is automatically enrolled into a Marketo nurture stream for both self-managed and SaaS products. The content of the nurture differs between a self-managed and a SaaS trial. Details of the self-managed Ultimate Nuture can be found in this [Google presentation](https://docs.google.com/presentation/d/1KSAZFwz3nvSTIXOP8urGWW6dJWhtpawVKFcaoFLDPdg/edit#slide=id.g2ae1ad1112_0_22) (must be signed into GitLab account to view). 

Details of SaaS trial are being finalized; you can follow the issue [here](https://gitlab.com/groups/gitlab-com/marketing/-/epics/98).

You have access to and are expected to use (and customize) the [Ultimate master sequences](https://app1a.outreach.io/sequences?smart_view=202) in Outreach for measureable follow up for Self-managed and SaaS trial records.

If a prospect requests a trial extension, we need to make sure that 1) we have had qualification questions answered and 2) they meet with an AE. 

### SDR Calling
As a best practice, each call should be prepared in advance by identifying which [Command of the Message](/handbook/marketing/product-marketing/sales-resources/#command-of-the-message) techniques are required to reach your goal for the call. 

Research your call in advance to have actionable value and input for the prospect. Make the other person feel that you actually know him and that he is not just a name on a list.

[Video: Best Practices for Opening Calls](https://www.youtube.com/watch?v=jR1RGSBHo5Q&feature=youtu.be)

[Video: Best Practices for Asking Questions](https://www.youtube.com/watch?v=Lv4nozScm-M)

- Call about them, not about you
- Be confident and passionate
- Aim for every role but focus on technical decision makers
- Ask for time
- Focus on your endgame: Sales Accepted Opportunities (SAO)
- Make it easy to say yes
- Obtain a commitment - [Video: Best Practices for Closing for Appointments](https://www.youtube.com/watch?v=FfUfmguwki4&feature=youtu.be)

### SDR Prospecting Call Structure
- Introduction
    - Immediately introduce yourself and GitLab
    - Ask for time
- Recognize the inquiry
- Qualification
- Inform them/answer questions about GitLab
- Commitments

### Warm Prospecting Call Example
Purpose: To qualify leads faster and more accurately (Sales Acceptance Criteria) than in an email exchange.

Process: In your reply message to set up/initiate a call, ask a few of your normal SDR questions to prep for the call. To save a step in emails include your Calendly link so leads may schedule the call directly.

    “Hi (lead name), this is (SDR) from GitLab. How are you today?”

    “Great, is now still a good time to talk about (primary issue)?”

After you’ve established the conversation is good to move forward, ask questions and guide the conversation in a way that enables the lead to tell you what their issue/problem is while also providing answers to the sales acceptance criteria. Your primary role is to gather information, and decide more appropriately how to provide assistance and/or qualify the lead for a call with an AE.

    “Great (lead name), thank you for sharing that information. I have a better feel now for how to move forward with your request/issue. I’m going to follow-up with an email recapping what we discussed, send over that documentation I promised and get you in touch with (account executive)”
    
There are some opportunities where it makes sense for us to have the prospect purchase through our online portal directly without engaging with an account executive however we would like to have an AE involved for opportunities where the 1) Company has 30 or more employees, or the 2) Opportunity value is at or above 3K or 3) our top 2 tiers are being considered or 4) a trial extension is being requested. After gathering SAO critera, please direct them to a meeting with an AE as the next step in our qualification process. As always, our goal should aways be to ensure the best customer journey so please make sure the next step makes the most sense for the prospect.

### Qualifying
[Video: Best Practices for Closing for Appointments](https://www.youtube.com/watch?v=FfUfmguwki4&feature=youtu.be)  
Your goal is to generate Sales Accepted Opportunities (SAOs) by gathering all pertinent information needed for an Account Executive to take action. Some examples of sales qualification questions can be found [here](/handbook/sales-qualification-questions/).


## How to SDR

[Video: Welcome to the GitLab Sales Development Team](https://www.youtube.com/watch?v=ESZtmKUn9QY&feature=youtu.be)

As an Sales Development Representative (SDR), you will be responsible for one of the most difficult and important parts of growing a business: outbound prospecting. You play a crucial role that helps bridge the gap between sales and marketing. You will be tasked with generating sales accepted opportunities within our large account segment.

Getting started, you'll work closely with your Sales Rep to strategize which accounts to focus on as well as the potential entry points into those accounts. From there you will work on your own to further research and understanding the account, their business, their initiatives and other related details. Next you will hunt for and identify prospects that have a role in making software development tooling decisions as well as the various avenues through which you could communicate with these prospects. Utilizing all of this research you’ll then strategically communicate GitLab's value with your prospects, as well as the value of taking a meeting with GitLab for a product overview.


### SDR Expectations

[Video: SDR Expectations & Metrics](https://www.youtube.com/watch?v=h2zd8BYQwhM)
* Meet monthly quota of Sales Accepted Opportunities (SAOs)
* Exceptional Salesforce hygiene, logging all prospecting activity, opportunity creation
* Maintaining a high sense of autonomy to focus on what's most important; created and sales accepted opportunities
* Attendance in each initial qualifying meeting (IQM) created - documenting notes and communicating with your SAL after the meeting
  - It will be in your best interest to sit in on as many meetings as possible at different stages in the buying process to see how the SALs work with prospects beyond qualifying. The better you understand the SALs you work with, the better you will be at making them successful through outbound prospecting support.

### Working with Sales

Sales Reps (Strategic, Midmarket, and SMB) receive support from the SDR team. Meeting cadence consist of the following:

- **Initial kick-off meeting**: Time - 1 hour; Discuss strategy, accounts, and schedules
- **Weekly Status Meeting**: Time - 30 minutes; Discuss initial meetings, opportunities, and prospecting strategy
- **Monthly Recurring Strategy Meeting**: Time - 1 hour; Evaluate strategy and opportunities

Additional ad-hoc meetings may be scheduled in addition to the above.

Sales Reps have direct impact on the Outbound SDR's success. It is very important to form a strong partnership to as you begin your prospecting efforts. The SALs you work with will know a lot about the accounts you are prospecting into, so be sure to communicate regularly to ensure you are as effective as possible.

### Preparing for Field Events
The SDR team will assist the Field Marketing team through event promotion and follow up. To learn more about Field Marketing, the types of programs they run and what is coming up, go to their handbook page [here](/handbook/marketing/revenue-marketing/field-marketing/). When Field Marketing begins preparing for an event, they will create an issue using one of their [regional issue templates](https://gitlab.com/gitlab-com/marketing/field-marketing/tree/master/.gitlab/issue_templates). Within that template is a section titled ‘Outreach and Follow Up’ for the SDR team to understand what is needed from them for each event. 

#### Process
1. The Field Marketing Manager will create the issue and fill out the goals in the section, ‘Outreach and Follow Up’. They will then mention the appropriate SDR manager based on the region of the event.
1. The SDR manager will select one SDR rep from their team to be the project lead for this event and mention them in the issue.
1. The project lead will read through the goals outlined by the FMM and begin completing them. 
1. Once an action is finished, the SDR rep will review it with their manager and then edit the description of the issue to link to the completed action item. 
1. The project lead will send the sequence to @megan_odowd to add both sequences to the Outreach collection.
1. SDR Manager and project lead will then connect on the date to launch the sequence and enable any other SDR reps assisting with this.
1. SDR project lead to announce the follow up sequence availability in the relevant SDR slack channels. 

#### Deadlines
**Event Promotion**
- The SDR manager needs to be assigned a minimum of 30-days prior to the event, but the sooner they can start prepping, the better.
- The SDR project lead will have one week to pull a list of people for event promotion and create or clone an event promotion sequence. The sequence will run for the next three weeks prior to the event. 

**Event Follow Up**
- The follow up sequence needs to be created at minimum of one week before the event date. 

**Sequence Information**
- Please clone one of the Master Event Sequences found in the [Events Outreach collection](https://app1a.outreach.io/sequences?direction=desc&order=recent&content_category_id%5B%5D=6) and pre populate as many of the variables as possible. If all of the variables can be populated manually or through Outreach/Salesforce, change the email type from ‘manual’ to ‘auto.’ Work with your manager and the FMM to make any other additional adjustments that may be needed.

## SDR Compensation and Quota

SDR’s total compensation is based on two key metrics:
* [Sales Accepted Opportunities (SAOs)](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao)
* Closed won business from SAOs - 1% commission for any closed won opportunity produced, **so as long as the rep is employed as a SDR by GitLab.**

Quota is based solely on SAOs. Think of the 1% commission on opportunities you source that a Strategic Account Leader subsequently closes and wins, as an added bonus. To be paid on your closed won business, your activity should clearly depict that your prospecting work sourced the opportunity.

Monthly quota of Sales Accepted Opportunities - (SAOs):

SDR Ramp Period
*  Month 1 - 0% of Sales Accepted Opportunities - (SAOs)  
    *  During your first month of employment you will be paid your OTE Commission by completing the GitLab SDR Onboarding assessments and issue. 
*  Month 2 - 50% Sales Accepted Opportunities - (SAOs)  
*  Month 3 onwards - 100% Sales Accepted Opportunities - (SAOs)  


There is an accelerator for SDRs who deliver over their SAO quota. SAOs are paid out based on progressive tiers of quota attainment:

* Base tier, 0-100%: Base Rate Payment (Monthly Variable @ Target/quota)
* Accelerator 1, 101%-150%: 1.1 times Base Rate Payment in excess of Quota
* Accelerator 2, > 150%: 1.5 times Base Rate Payment of quota

* During the Ramp Period (Months 1-2) once the quota has been achieved, each additional opportunity is paid at the Month 3 base rate.

Below are reports from Salesforce that GitLab leadership uses to see all the SDR created and sales accepted opportunities. If you believe an opportunity you sourced is not reflected in the reporting, notify your manager.

* [Outbound SDR created opportunities](https://gitlab.my.salesforce.com/00O61000003nmhe)
* [Outbound SDR sales accepted opportunities](https://gitlab.my.salesforce.com/00O61000003nmhU?dbw=1)

**SDRs will also be measured on the following:**

* Results
  * Pipeline value of SDR sourced opportunities
  * IACV won from opportunities SDR sources
* Activity
  * % of named accounts contacted
  * Number of opportunities created
  * Number of calls made
  * Number of personalized emails sent

The above measurements do not impact your quota attainment, but are critical to being a successful SDR. A SDR's main focus should always be achieving their SAO quota.

## SDR Account Research
* Now that you have a book of business, the next step is determining which accounts and roles at those companies you should approach first. This can be determined in conjunction with your assigned account executive or SDR Manager. Chances are they may have prospected into the account in the past or have some telling information about the account. Weekly syncs on assigned accounts will directly impact your success as an SDR. To be prepared for that weekly sync and your everyday prospecting it helps to do the following:
 * Create a targeted list; look for key indicators like number of existing contacts, historical opportunities, CE usage, and EE customers
 * Within that list, identify the buyers based on GitLab's buyer personas, keep the list short and simple for each account
 * Update your chosen target accounts in Salesforce by looking for any missing data points such as phone numbers, emails, LinkedIn profiles, contacts etc.
 * Look for contacts with IT leadership and application development leadership responsibilities. This can be senior technical individual contributors such as software architects, or leaders in the organization's IT department all the way up to a CIO or CTO.
 * Filter in, filter out. Alternate your target accounts from your main 30 accounts to keep you going.

Leveraging the data gathered above, a SDR should spend no more than three minutes performing account research to personalize messaging. Below is an outline to help you come up with a personalized message in three minutes or less

**Personalized message research help and example:**

Three things in three minutes about the Organization, Industry, Prospect

**Salesforce**
  * Locate the account in Salesforce
  * Are there other leads that are being worked?
  * Is this a customer account?
  * Are there any open or closed opportunities? Are we able to reference these?
  * Activity history
  * Who in the organization have we been in contact with? Can we follow up?

**LinkedIn**
  * Refer to the prospect's summary. Is there any mutual interest you share?
  * Does anything stand out that is relevant to their needs as an organization?
  * What is the prospect's role and how does that affect your messaging?
  * Have they published any articles that would be work referencing?
  * Work history, any GitLab customers that we can reference?
  * Personalization: Any mutual Hobbies/Interests you share? Location? Education?
  * Connections, are they connected to anyone at GitLab for a possible introduction? If so, please slack that individual to see if they would be willing to make a introduction

**Company Website**
  * Mission statement
  * Press releases/Newsroom
  * Major IT Initiatives
  * (ctrl F) search for keywords
  * Navigate to "Jobs" page and look for job description for developers, look what tools they require to hire a developer, this will give you and idea of how their tool stack can look like.

## Outbound Prospecting - The Outreach
At GitLab, the top performing SDRs consistently make and send out a high volume of phone calls and personalized emails in their prospecting efforts. Social Selling and general creativity around prospecting activities is highly encouraged and often leads to more success. In example; SDRs will all be provided a license to Vidyard [(download it here)](https://www.vidyard.com/govideo/).

In the spirit of GitLab's vision: "Everyone can contribute", GitLab SDR's will be expected lead, participate and contribute in regular discussions around new creative ways to engage our prospective customers. When calling into Europe, be mindful of GDPR and only email contacts whose `GDPR Compliant` boxes are checked in SFDC; Also, all Discover.Org contacts are GDPR opt in.

[Video: Sample Vidyard Video for a prospect](https://share.vidyard.com/watch/HyoPfVvUJtpduNSXYtbQgP)

[Video: Best Practices for Opening Calls](https://www.youtube.com/watch?v=jR1RGSBHo5Q&feature=youtu.be)

[Video: Best Practices for Asking Questions](https://www.youtube.com/watch?v=Lv4nozScm-M)

### Cold Calling

With all the technology available to you as a SDR here at GitLab there really isn't such a thing as a "cold" call. Below are a few important tips that will help you structure a prospecting call:
* Call about them, not you. Don't be what everyone thinks a salesperson is, don't throw up on them with cool information about GitLab
* Always have their LinkedIn profile up
* Listen. Provide value for them to continue conversations with GitLab
* Ensure that you are calling at the right times. Data suggests in the morning, at lunch, and in the evening as best call practices. Schedule your day accordingly.
* Be confident and passionate
* Focus on decision makers, but don't leave the other roles hanging
* Practice with other SDRs or your Team Lead on objection handling
* Obtain a commitment

### SDR Prospecting Call Structure
* Introduction
    * Immediately introduce yourself and GitLab
    * State intention to book a meeting
    * Ask for time
* Initial benefit statement
* Qualification, ask questions, listen
* Obtain the commitment, ask for the meeting, come prepared with a few times off hand that work for you and be specific

### SDR Objection Handling
[Video: Objection Handling Techniques](https://www.youtube.com/watch?v=Q_RnXedP0JI)

**Dealing with rejection over the phone**
* You will be told "no" a lot. Don't let this bother you, ask yourself after every call; "What could I have done better or differently?" Then gather yourself and approach the next call.
* Do we end the call when we hear the first objection? NO! If you have someone on the phone already don't lose the chance to have a conversation. Below is a guide for navigating objections in prospecting calls called the *3 No Rule*

**3 No Rule**

**A- Approach/Intro**
* Hi (prospect's name), this is (your name) calling with GitLab
    * ...I know you weren't expecting my call... so I will be brief OR ...do you have a few minutes?
    * ...Did I catch you at an okay time?
    * ...have you heard of GitLab?

**B- Brief Initial Benefit Statement (The Why)**
* I recently (read/saw/spoke to/understand) that (something you found in your pre call research)...
    * The purpose of my call is...
    * ...While I have you on the phone, I had a couple of quick questions...

**C- Connect/Ask Questions**
* Have you heard of GitLab?
    * "No" - Tell them
    * "Yes"
        * Where did you hear about it?
        * Have you used it before?
    * What are you using for... (topic of your initial benefit statement)?
    * What do you like about X tool?
    * Anything you would change about X tool if you could?

**D- Why Now?**
* After you have found an area to add value to:
    * Give initial benefit statement (focusing on how we can help them, give examples, share case studies)
    * Recommend next steps based on identified need, pain, and goals
    * Be confident, assumptive, and consultative

**E- Obtaining the commitment** - [Video: Best Practices for Closing for Appointments](https://www.youtube.com/watch?v=FfUfmguwki4&feature=youtu.be)
* Meeting first - If they object, bring up the need, pain and goals that you have identified, reposition yourself and ask again if they could get X value, would it be worth a 5-10 minute call?
* Why not?
    * if still a "no" suggest they attend or watch a recorded webinar on our product
        * If still a "no", recommend a case study or article. Anything that you can follow up with them on

**F- Follow up**
* Send a recap email with what you told them you would send
* Record the notes about your conversation in Salesforce and set tasks for future follow up
