---
layout: markdown_page
title: "Category Vision - Tracing"
---

- TOC
{:toc}

# Tracing

| | |
| --- | --- |
| Stage | [Monitor](/direction/monitor/) |
| Maturity | [Minimal](/direction/maturity/) |

## Introduction and how you can help
Thanks for visiting this category strategy page on Tracing in GitLab. This category belongs to and is maintained by the [APM](/handbook/engineering/development/ops/monitor/APM/) group of the Monitor stage.

This strategy is a work in progress, and everyone can contribute:
 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aapm&label_name[]=Category%3ATracing) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aapm&label_name[]=Category%3ATracing) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your Metrics usage, we'd especially love to hear your use case(s)  
 

## Background    
Today we have metrics, which provides information like how often a particular event occurs or the quantity of a resource being consumed. This is great for things like tracking the how long it takes to respond to requests, how much CPU is consumed, etc.
What we are missing is deeper insight into the application. To continue with the example we may know that on average requests are serviced around 350ms. What we don't know is why. Is it a long database call? Is an underlying service slow to respond? Is there a blocking operation causing resource contention?
These questions can be answered with Tracing, which provides an in-depth look at a particular request, tracing its flow from initial reception to response to the service.

## What's Next & Why
We're pursuing continued iteration on our initial [Tracing MVC](https://gitlab.com/groups/gitlab-org/-/epics/89) with a specific next step to [enable deployment of Jaeger to your Kubernetes cluster](https://gitlab.com/gitlab-org/gitlab-ee/issues/5182). 

## Maturity Plan
* [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/837)

## Competitive Landscape
Not yet, but accepting merge requests to this document.

## Analyst Landscape
Not yet, but accepting merge requests to this document.

## Top Customer Success/Sales Issue(s)
Not yet, but accepting merge requests to this document.

## Top Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Internal Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Vision Item(s)
Not yet, but accepting merge requests to this document.
